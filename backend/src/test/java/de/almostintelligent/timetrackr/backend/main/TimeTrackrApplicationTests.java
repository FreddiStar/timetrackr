package de.almostintelligent.timetrackr.backend.main;

import de.almostintelligent.timetrackr.backend.TimeTrackrApplication;
import org.junit.Test;
import org.springframework.boot.test.SpringApplicationConfiguration;

@SpringApplicationConfiguration(classes = TimeTrackrApplication.class)
public class TimeTrackrApplicationTests {

	@Test
	public void contextLoads() {
	}

}
