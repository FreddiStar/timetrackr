package de.almostintelligent.timetrackr.backend.controllers;

import de.almostintelligent.timetrackr.backend.config.ApiConfig;
import de.almostintelligent.timetrackr.backend.repositories.CategoryRepository;
import de.almostintelligent.timetrackr.backend.repositories.TrackedTimeRepository;
import de.almostintelligent.timetrackr.models.Category;
import de.almostintelligent.timetrackr.models.TrackedTime;
import de.almostintelligent.timetrackr.models.requests.StartTimeRequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(ApiConfig.API_PREFIX + "/trackedtime")
public class TrackedTimeController {

    private TrackedTimeRepository trackedTimeRepository;
    private CategoryRepository categoryRepository;

    @Autowired
    public TrackedTimeController(TrackedTimeRepository timeRepository,
                                 CategoryRepository categoryRepository) {
        this.trackedTimeRepository = timeRepository;
        this.categoryRepository = categoryRepository;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<TrackedTime> getTrackedTimes() {
        return trackedTimeRepository.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public TrackedTime getTrackedTimes(@PathVariable("id") Integer id) {
        return trackedTimeRepository.findOne(id);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public TrackedTime updateTracking(@RequestBody TrackedTime time,
                                      HttpServletResponse reponse) {
        trackedTimeRepository.update(time);
        return time;
    }

    @RequestMapping(value = "/stop", method = RequestMethod.POST)
    public TrackedTime stopTracking(@RequestBody TrackedTime time,
                                    HttpServletResponse response) throws IOException {

        TrackedTime t = trackedTimeRepository.findOne(time.getId());

        if (t != null) {
            if (t.getEndTime() == null) {
                t.setEndTime(new Date().getTime());
                trackedTimeRepository.update(t);
                return t;
            } else {
                response.sendError(HttpStatus.CONFLICT.value(),
                        "Time already stopped!");
                return null;
            }
        } else {
            response.sendError(HttpStatus.NOT_FOUND.value());
            return null;
        }
    }

    @RequestMapping(value = "/start", method = RequestMethod.POST)
    public TrackedTime startTracking(@RequestBody StartTimeRequestBody body,
                                     HttpServletResponse response) throws IOException {

        if (body != null) {

            List<Category> categories = categoryRepository.findByIds(body.categories);

            TrackedTime time = new TrackedTime();
            time.addAllCategories(categories);
            time.setStartTime(new Date().getTime());
            time.setDescription("Test Desc.");

            trackedTimeRepository.create(time);

            response.setHeader("Location", ApiConfig.API_PREFIX
                    + "/trackedtime/" + time.getId());
            response.setStatus(HttpStatus.CREATED.value());
            return time;

        } else {
            response.sendError(HttpStatus.BAD_REQUEST.value(), "BAD REQUEST");
            return null;
        }

    }

}
