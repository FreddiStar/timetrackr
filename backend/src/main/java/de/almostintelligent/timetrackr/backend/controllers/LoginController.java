package de.almostintelligent.timetrackr.backend.controllers;

import de.almostintelligent.timetrackr.backend.config.ApiConfig;
import de.almostintelligent.timetrackr.backend.repositories.UserRepository;
import de.almostintelligent.timetrackr.models.User;
import de.almostintelligent.timetrackr.models.requests.LoginRequest;
import de.almostintelligent.timetrackr.models.responses.LoginResponse;
import org.apache.tomcat.util.security.MD5Encoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@RestController
@RequestMapping(ApiConfig.API_PREFIX)
public class LoginController {

    private UserRepository userRepository;

    @Autowired
    public LoginController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public LoginResponse login(@RequestBody LoginRequest request,
                               HttpServletRequest httpRequest,
                               HttpServletResponse response) throws IOException, NoSuchAlgorithmException {

        User u = userRepository.getByUsernameAndPassword(request.getUsername(), request.getPassword());

        if (u != null) {
            return new LoginResponse(generateToken(u, httpRequest.getRemoteAddr()));
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
    }

    private String generateToken(User u, String ip) throws NoSuchAlgorithmException {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        String rawToken = u.getUsername() + ":"
                + MD5Encoder.encode(md5.digest(ip.getBytes())) + ":"
                + MD5Encoder.encode(md5.digest(u.getPassword().getBytes()));
        return Base64.getEncoder().encodeToString(rawToken.getBytes());
    }

}
