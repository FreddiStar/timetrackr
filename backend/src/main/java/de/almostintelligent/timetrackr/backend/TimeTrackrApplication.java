package de.almostintelligent.timetrackr.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = { "de.almostintelligent",
		"de.almostintelligent.controllers", "de.almostintelligent.config" })
@Configuration
@EnableAutoConfiguration
public class TimeTrackrApplication {

	public static void main(String[] args) {
		SpringApplication.run(TimeTrackrApplication.class, args);
	}
}
