package de.almostintelligent.timetrackr.backend.repositories;

import java.util.List;

public interface BaseRepository<T, I> {

	void create(T t);

	void update(T t);

	void delete(T t);

	List<T> findAll();

	T findOne(I id);
	
	void deleteAll();

}
