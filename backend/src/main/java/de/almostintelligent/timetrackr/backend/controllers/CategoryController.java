package de.almostintelligent.timetrackr.backend.controllers;

import de.almostintelligent.timetrackr.backend.config.ApiConfig;
import de.almostintelligent.timetrackr.backend.repositories.CategoryRepository;
import de.almostintelligent.timetrackr.models.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(ApiConfig.API_PREFIX + "/category")
public class CategoryController {

    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryController(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @RequestMapping("")
    public List<Category> getCategories() {
        return categoryRepository.findAll();
    }

    @RequestMapping("/{id}")
    public Category getTag(@PathVariable("id") Integer id) {
        return categoryRepository.findOne(id);
    }
}
