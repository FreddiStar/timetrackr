package de.almostintelligent.timetrackr.backend.repositories;


import de.almostintelligent.timetrackr.models.Project;

public interface ProjectRepository extends BaseRepository<Project, Integer> {

    void deleteAll();

}
