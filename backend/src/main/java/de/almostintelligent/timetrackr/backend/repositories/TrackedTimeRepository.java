package de.almostintelligent.timetrackr.backend.repositories;


import de.almostintelligent.timetrackr.models.TrackedTime;

public interface TrackedTimeRepository extends
        BaseRepository<TrackedTime, Integer> {
}
