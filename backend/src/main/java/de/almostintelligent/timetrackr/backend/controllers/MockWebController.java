package de.almostintelligent.timetrackr.backend.controllers;

import de.almostintelligent.timetrackr.backend.repositories.CategoryRepository;
import de.almostintelligent.timetrackr.backend.repositories.ProjectRepository;
import de.almostintelligent.timetrackr.backend.repositories.TrackedTimeRepository;
import de.almostintelligent.timetrackr.backend.repositories.UserRepository;
import de.almostintelligent.timetrackr.models.Category;
import de.almostintelligent.timetrackr.models.Project;
import de.almostintelligent.timetrackr.models.TrackedTime;
import de.almostintelligent.timetrackr.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Controller
public class MockWebController {

    private CategoryRepository categoryRepository;
    private UserRepository userRepository;
    private TrackedTimeRepository trackedTimeRepository;
    private ProjectRepository projectRepository;

    @Autowired
    public MockWebController(CategoryRepository categoryRepository,
                             UserRepository userRepository,
                             ProjectRepository projectRepository,
                             TrackedTimeRepository trackedTimeRepository) {
        this.categoryRepository = categoryRepository;
        this.userRepository = userRepository;
        this.trackedTimeRepository = trackedTimeRepository;
        this.projectRepository = projectRepository;
    }

    private void clearDB() {
        trackedTimeRepository.deleteAll();
        categoryRepository.deleteAll();
        userRepository.deleteAll();
    }

    private void createUsers(int count) {
        for (int i = 1; i <= count; ++i) {
            User u = new User();
            u.setCreatedAt(new Date().getTime());
            u.setEmail("test@test.de");
            u.setUsername("User " + i);
            u.setPassword("password");

            userRepository.create(u);
        }
    }

    private void createProjects(int count) {
        for (int i = 1; i <= count; ++i) {
            Project project = new Project();

            project.setCreatedAt(new Date().getTime());
            project.setName("Project " + i);

            projectRepository.create(project);
        }
    }

    private void createCategories(int count) {

        List<Project> projects = projectRepository.findAll();

        for (Project p : projects) {
            for (int i = 1; i <= count; ++i) {
                Category cat = new Category();
                cat.setName("Category " + i);
                cat.setCreatedAt(new Date().getTime());
                cat.setProject(p);

                categoryRepository.create(cat);

                p.addCategory(cat);
                projectRepository.update(p);
            }
        }
    }

    private void createTimes(int count) {
        List<User> users = userRepository.findAll();
        List<Project> projects = projectRepository.findAll();

        Random random = new Random();

        for (User user : users) {
            for (Project p : projects) {
                for (int c = 0; c < count; ++c) {
                    List<Category> categories = new ArrayList<>();
                    categories.addAll(p.getCategories());

                    TrackedTime time = new TrackedTime();
                    time.setUser(user);

                    time.setProject(p);
                    time.setDescription("Some Description");

                    time.setStartTime(1425670698000L);
                    time.setEndTime(1425670698000L + 3600000L);

                    trackedTimeRepository.create(time);

                    for (int i = 0; i < 2; ++i) {
                        Category category = categories.get(random.nextInt(categories.size()));
                        category.addTrackedTime(time);
                        categoryRepository.update(category);
                    }

                    p.addTrackedTime(time);
                }

                projectRepository.update(p);
            }
        }
    }

    @RequestMapping("/mock")
    public ModelAndView createMocks() {

        clearDB();

        createProjects(10);
        createCategories(10);
        createUsers(5);

        createTimes(5);

        ModelAndView mav = new ModelAndView("index");
        mav.addObject("view", "mock_overview");
        return mav;
    }

}
