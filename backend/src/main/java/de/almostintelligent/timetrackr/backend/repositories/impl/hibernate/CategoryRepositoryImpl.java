package de.almostintelligent.timetrackr.backend.repositories.impl.hibernate;

import de.almostintelligent.timetrackr.backend.repositories.CategoryRepository;
import de.almostintelligent.timetrackr.backend.utils.HibernateQueryUtils;
import de.almostintelligent.timetrackr.models.Category;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional("transactionManager")
public class CategoryRepositoryImpl implements CategoryRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public CategoryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Category> findByIds(List<Integer> ids) {
        if (ids == null) {
            return new ArrayList<>();
        }

        return null;
    }

    @Override
    public void create(Category category) {
        sessionFactory.getCurrentSession().save(category);
    }

    @Override
    public void update(Category category) {
        sessionFactory.getCurrentSession().update(category);
    }

    @Override
    public void delete(Category category) {
        sessionFactory.getCurrentSession().delete(category);
    }

    @Override
    public List<Category> findAll() {
        return HibernateQueryUtils.listAll(Category.class, sessionFactory);
    }

    @Override
    public Category findOne(Integer id) {
        return HibernateQueryUtils.findOne(Category.class, sessionFactory, id);
    }

    @Override
    public void deleteAll() {
        List<Category> all = findAll();
        for (Category c : all) {
            delete(c);
        }
    }
}
