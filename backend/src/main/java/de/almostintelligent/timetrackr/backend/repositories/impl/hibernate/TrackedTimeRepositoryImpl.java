package de.almostintelligent.timetrackr.backend.repositories.impl.hibernate;

import de.almostintelligent.timetrackr.backend.repositories.TrackedTimeRepository;
import de.almostintelligent.timetrackr.backend.utils.HibernateQueryUtils;
import de.almostintelligent.timetrackr.models.TrackedTime;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional("transactionManager")
public class TrackedTimeRepositoryImpl implements TrackedTimeRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public TrackedTimeRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void create(TrackedTime p) {
        sessionFactory.getCurrentSession().save(p);

    }

    public List<TrackedTime> findAll() {
        return HibernateQueryUtils.listAll(TrackedTime.class, sessionFactory);
    }

    public TrackedTime findOne(Integer id) {
        return HibernateQueryUtils.findOne(TrackedTime.class, sessionFactory,
                id);
    }

    public void update(TrackedTime t) {
        sessionFactory.getCurrentSession().update(t);

    }

    public void delete(TrackedTime t) {
        sessionFactory.getCurrentSession().delete(t);
    }

    @Override
    public void deleteAll() {
        List<TrackedTime> list = findAll();

        for (TrackedTime p : list) {
            delete(p);
        }

    }
}
