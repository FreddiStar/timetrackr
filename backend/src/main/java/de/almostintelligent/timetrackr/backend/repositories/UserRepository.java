package de.almostintelligent.timetrackr.backend.repositories;

import de.almostintelligent.timetrackr.models.User;

public interface UserRepository extends BaseRepository<User, Integer> {

    User getByUsernameAndPassword(String username, String password);

}
