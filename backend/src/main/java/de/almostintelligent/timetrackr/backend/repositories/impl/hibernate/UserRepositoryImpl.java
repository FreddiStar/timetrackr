package de.almostintelligent.timetrackr.backend.repositories.impl.hibernate;

import de.almostintelligent.timetrackr.backend.repositories.UserRepository;
import de.almostintelligent.timetrackr.backend.utils.HibernateQueryUtils;
import de.almostintelligent.timetrackr.models.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional("transactionManager")
public class UserRepositoryImpl implements UserRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void create(User p) {
        sessionFactory.getCurrentSession().save(p);
    }

    public List<User> findAll() {
        return HibernateQueryUtils.listAll(User.class, sessionFactory);
    }

    public User findOne(Integer id) {
        return HibernateQueryUtils.findOne(User.class, sessionFactory, id);
    }

    public void update(User t) {
        sessionFactory.getCurrentSession().update(t);
    }

    public void delete(User t) {
        sessionFactory.getCurrentSession().delete(t);
    }

    @Override
    public void deleteAll() {
        List<User> list = findAll();

        for (User p : list) {
            delete(p);
        }
    }

    @Override
    public User getByUsernameAndPassword(String username, String password) {
        List<User> list = findAll();
        for (User u : list) {
            if (u.getUsername().equals(username) && u.getPassword().equals(password)) {
                return u;
            }
        }

        return null;
    }
}
