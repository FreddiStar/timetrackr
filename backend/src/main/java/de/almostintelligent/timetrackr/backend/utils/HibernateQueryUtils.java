package de.almostintelligent.timetrackr.backend.utils;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;

import java.util.List;

public class HibernateQueryUtils {

    public static <T> List<T> listAll(Class<T> type,
                                      SessionFactory sessionFactory) {
        @SuppressWarnings("unchecked")
        List<T> list = (List<T>) sessionFactory.getCurrentSession()
                .createCriteria(type)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        return list;
    }

    public static <T> List<T> findMany(Class<T> type, SessionFactory sessionFactory, List<Integer> ids) {
        StringBuilder builder = new StringBuilder();

        for (Integer id : ids) {
            builder.append(id);
            builder.append(",");
        }

        builder.deleteCharAt(builder.lastIndexOf(","));

        Query query = sessionFactory.getCurrentSession().createQuery(
                "select c from " + type.getName() + " c where c.id IN (:id)");
        query.setParameter("id", builder.toString());

        return (List<T>) query.list();
    }

    public static <T> T findOne(Class<T> type, SessionFactory sessionFactory,
                                Integer id) {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "select c from " + type.getName() + " c where c.id = :id");
        query.setParameter("id", id);

        @SuppressWarnings("unchecked")
        List<T> list = (List<T>) query.list();

        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }
}
