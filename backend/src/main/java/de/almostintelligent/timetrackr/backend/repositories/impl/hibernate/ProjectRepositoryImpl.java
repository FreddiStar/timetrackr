package de.almostintelligent.timetrackr.backend.repositories.impl.hibernate;

import de.almostintelligent.timetrackr.backend.repositories.ProjectRepository;
import de.almostintelligent.timetrackr.backend.utils.HibernateQueryUtils;
import de.almostintelligent.timetrackr.models.Project;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional("transactionManager")
public class ProjectRepositoryImpl implements ProjectRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public ProjectRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void create(Project p) {
        sessionFactory.getCurrentSession().save(p);
    }

    public List<Project> findAll() {
        return HibernateQueryUtils.listAll(Project.class, sessionFactory);
    }

    public Project findOne(Integer id) {
        return HibernateQueryUtils.findOne(Project.class, sessionFactory, id);
    }

    public void update(Project t) {
        sessionFactory.getCurrentSession().update(t);
    }

    public void delete(Project t) {
        sessionFactory.getCurrentSession().delete(t);
    }

    @Override
    public void deleteAll() {
        List<Project> list = findAll();

        for (Project p : list) {
            delete(p);
        }
    }

}
