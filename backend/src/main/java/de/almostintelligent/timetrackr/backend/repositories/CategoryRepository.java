package de.almostintelligent.timetrackr.backend.repositories;

import de.almostintelligent.timetrackr.models.Category;

import java.util.List;

public interface CategoryRepository extends BaseRepository<Category, Integer> {

    List<Category> findByIds(List<Integer> ids);
}
