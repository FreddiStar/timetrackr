package de.almostintelligent.timetrackr.backend.controllers;

import de.almostintelligent.timetrackr.backend.config.ApiConfig;
import de.almostintelligent.timetrackr.backend.repositories.ProjectRepository;
import de.almostintelligent.timetrackr.models.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping(ApiConfig.API_PREFIX + "/project")
public class ProjectController {

    private ProjectRepository projectRepository;

    @Autowired
    public ProjectController(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @RequestMapping()
    public List<Project> getProjects() {
        return projectRepository.findAll();
    }

    @RequestMapping("/{id}")
    public Project getProject(@PathParam("id") Integer id) {
        return projectRepository.findOne(id);
    }

}
