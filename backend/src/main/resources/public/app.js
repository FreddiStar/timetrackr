var API = 'http://localhost:8080/api/v1';

var TimeTrackrServices = angular.module('TimeTrackrServices', ['ngResource']);

ProjectRepository = function($resource) {
	return $resource(API + '/project/:id', {}, {
		all : {
			method : 'GET',
			params : {
				id : ''
			},
			isArray : true
		},
		one : {
			method : 'GET',
			params : {
				id : ''
			},
			isArray : false
		}
	});
}

TimeTrackrServices.factory('ProjectRepository', ['$resource', ProjectRepository]);

var TimeTrackrControllers = angular.module('TimeTrackrControllers', []);

HomeController = function($scope, $http) {
}

CategoryListController = function($scope, $http) {
	$http.get(API + '/category').success(function(data) {
		$scope.categories = data;
	});
}

UserListController = function($scope, $http) {
	$http.get(API + '/user').success(function(data) {
		$scope.users = data;
	});
}

ProjectListController = function($scope, $http, ProjectRepository) {
	$scope.projects = ProjectRepository.all();
}

ProjectDetailController = function($scope, $http, $routeParams, ProjectRepository) {
	$scope.project = ProjectRepository.one({
		id : $routeParams.id,
	});
}

TimeTrackrControllers.controller('HomeController', ['$scope', '$http', HomeController]);
TimeTrackrControllers.controller('ProjectListController', ['$scope', '$http', 'ProjectRepository', ProjectListController]);
TimeTrackrControllers.controller('ProjectDetailController', ['$scope', '$http', '$routeParams', 'ProjectRepository', ProjectDetailController]);
TimeTrackrControllers.controller('UserListController', ['$scope', '$http', UserListController]);
TimeTrackrControllers.controller('CategoryListController', ['$scope', '$http', CategoryListController]);

var TimeTrackrApp = angular.module('TimeTrackrApp', ['ngRoute', 'TimeTrackrControllers', 'TimeTrackrServices']);

TimeTrackrApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/home', {
		templateUrl : 'views/home.html',
		controller : 'HomeController'
	}).when('/project', {
		templateUrl : 'views/project_details.html',
		controller : 'ProjectDetailController'
	}).when('/project/:id', {
		templateUrl : 'views/project_list.html',
		controller : 'ProjectListController'
	}).when('/user', {
		templateUrl : 'views/user_list.html',
		controller : 'UserListController'
	}).when('/category', {
		templateUrl : 'views/category_list.html',
		controller : 'CategoryListController'
	}).otherwise({
		redirectTo : '/home'
	})
}]);