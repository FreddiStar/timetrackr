package de.almostintelligent.timetrackr.client.controller;

import javafx.application.Platform;

/**
 * Created by Freddi on 06.04.2015.
 */
public class BaseController {

    public void GUI(Runnable r) {
        Platform.runLater(r);
    }

}
