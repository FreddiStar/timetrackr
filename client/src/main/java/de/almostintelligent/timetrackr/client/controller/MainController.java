package de.almostintelligent.timetrackr.client.controller;

import de.almostintelligent.timetrackr.client.Main;
import de.almostintelligent.timetrackr.client.api.BackendApi;
import de.almostintelligent.timetrackr.models.TrackedTime;
import de.almostintelligent.timetrackr.models.requests.StartTimeRequestBody;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MainController extends BaseController {

    @FXML
    private VBox mainRoot;

    @FXML
    private Button btnStart;

    @FXML
    private Label updateText;

    public static final String TIME_FORMAT = "hh:mm:ss";

    private boolean started = false;

    private TrackedTime trackingTime;
    private Timer updateTimer;

    public MainController() {
        updateTimer = new Timer();
    }

    @FXML
    private void initialize() {

        initStartButton();

    }

    private void initStartButton() {
        btnStart.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (started) {
                    stopTimeTracking();
                } else {
                    startTimeTracking();
                }
            }
        });
    }

    private void startUpdateTimer() {
        updateTimer = new Timer();
        updateTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                updateTrackingText();
            }
        }, 0, 1000);
    }

    private void updateTrackingText() {

        DateFormat date = new SimpleDateFormat(TIME_FORMAT);
        Date now = new Date();
        Long timeDiffInSec = (now.getTime() - trackingTime.getStartTime()) / 1000;

        StringBuilder builder = new StringBuilder();

        long hours = timeDiffInSec / 3600;
        long minutes = timeDiffInSec / 60 % 60;
        long seconds = timeDiffInSec % 60;

        builder.append(date.format(new Date(trackingTime.getStartTime())));
        builder.append(" - ");
        builder.append(date.format(now));
        builder.append(" (");
        builder.append(hours);
        builder.append("h ");
        builder.append(String.format("%02d", minutes));
        builder.append("m ");
        builder.append(String.format("%02d", seconds));
        builder.append("s)");

        GUI(() -> {
            updateText.setText(builder.toString());
        });
    }


    private void stopUpdateTimer() {
        updateTimer.cancel();
        updateTimer.purge();
        updateTimer = null;
    }

    private void stopTimeTracking() {
        btnStart.setDisable(true);
        btnStart.setText("Start");

        stopUpdateTimer();

        started = false;

        trackingTime.setEndTime(new Date().getTime());
        BackendApi.get().stopTimeTracking(trackingTime, new Callback<TrackedTime>() {
            @Override
            public void success(TrackedTime trackedTime, Response response) {
                GUI(() -> {
                    btnStart.setDisable(false);
                    updateText.setText("");
                });
            }

            @Override
            public void failure(RetrofitError error) {
                GUI(() -> {
                    btnStart.setDisable(false);
                    updateText.setText("");
                });
            }
        });

    }

    private void startTimeTracking() {
        btnStart.setDisable(true);
        btnStart.setText("Stop");

        StartTimeRequestBody body = new StartTimeRequestBody();
        body.categories = null;

        BackendApi.get().startTimeTracking(body, new Callback<TrackedTime>() {
                    @Override
                    public void success(TrackedTime trackedTime, Response response) {
                        trackingTime = trackedTime;
                        started = true;

                        startUpdateTimer();

                        GUI(() -> {
                            btnStart.setDisable(false);
                        });
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        started = false;

                        stopUpdateTimer();

                        GUI(() -> {
                            btnStart.setDisable(false);
                        });
                    }
                }

        );
    }

}
