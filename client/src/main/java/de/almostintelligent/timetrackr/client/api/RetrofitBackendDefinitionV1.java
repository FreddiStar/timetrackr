package de.almostintelligent.timetrackr.client.api;

import de.almostintelligent.timetrackr.models.TrackedTime;
import de.almostintelligent.timetrackr.models.requests.LoginRequest;
import de.almostintelligent.timetrackr.models.requests.StartTimeRequestBody;
import de.almostintelligent.timetrackr.models.responses.LoginResponse;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;

import java.util.List;

/**
 * Created by frederiktrojahn on 21.03.15.
 */
public interface RetrofitBackendDefinitionV1 {

    static final String API_V1 = "/api/v1";

    @POST(API_V1 + "/login")
    void login(@Body LoginRequest login, Callback<LoginResponse> cb);

    //@POST(API_V1 + "/logout")
    //void logout();

    @GET(API_V1 + "/trackedtime")
    void getTrackedTimes(Callback<List<TrackedTime>> cb);

    @POST(API_V1 + "/trackedtime/start")
    void startTimeTracking(@Body StartTimeRequestBody body, Callback<TrackedTime> cb);

    @POST(API_V1 + "/trackedtime/stop")
    void stopTimeTracking(@Body TrackedTime body, Callback<TrackedTime> cb);

    @PUT(API_V1 + "/trackedTime")
    void updateTrackedTime(@Body TrackedTime time, Callback<TrackedTime> cb);
}
