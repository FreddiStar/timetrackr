package de.almostintelligent.timetrackr.client.controller;

import de.almostintelligent.timetrackr.client.api.BackendApi;
import de.almostintelligent.timetrackr.client.app.TimeTrackrClientApplication;
import de.almostintelligent.timetrackr.client.utils.NodeUtils;
import de.almostintelligent.timetrackr.models.requests.LoginRequest;
import de.almostintelligent.timetrackr.models.responses.LoginResponse;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LoginController {

    @FXML
    private Button btnLogin;

    @FXML
    private VBox loginRoot;

    @FXML
    private ProgressIndicator loginIndicator;

    @FXML
    private PasswordField password;

    @FXML
    private TextField username;

    @FXML
    private void initialize() {
        btnLogin.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loginIndicator.setVisible(true);
                NodeUtils.setDisableAll(true, btnLogin, password, username);

                BackendApi.get().login(new LoginRequest(username.getText(), password.getText()), new Callback<LoginResponse>() {
                    @Override
                    public void success(LoginResponse loginResponse, Response response) {
                        TimeTrackrClientApplication.getInstance().getAppController().removeLayer(loginRoot);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Platform.runLater(() -> {
                            loginIndicator.setVisible(false);
                            NodeUtils.setDisableAll(false, btnLogin, password, username);
                        });
                    }
                });
            }
        });

    }
}
