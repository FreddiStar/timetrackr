package de.almostintelligent.timetrackr.client;

import de.almostintelligent.timetrackr.client.app.TimeTrackrClientApplication;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

    public static void main(String[] args) {
        Application.launch(Main.class, args);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("app.fxml"));
            Parent appRoot = loader.load();
            TimeTrackrClientApplication.getInstance().setAppController(loader.getController());

            primaryStage.setScene(new Scene(appRoot));
            primaryStage.setTitle("TimeTrackr");

            primaryStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
