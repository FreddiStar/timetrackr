package de.almostintelligent.timetrackr.client.utils;

import javafx.scene.Node;

/**
 * Created by frederiktrojahn on 28.03.15.
 */
public class NodeUtils {

    public static void setDisableAll(boolean disable, Node... nodes) {
        for (Node n : nodes) {
            n.setDisable(disable);
        }
    }

}
