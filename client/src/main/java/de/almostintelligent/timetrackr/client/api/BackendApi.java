package de.almostintelligent.timetrackr.client.api;

import retrofit.RestAdapter;

/**
 * Created by frederiktrojahn on 21.03.15.
 */
public class BackendApi {

    public static RetrofitBackendDefinitionV1 get() {
        RestAdapter adapter = new RestAdapter
                .Builder()
                .setEndpoint("http://localhost:8080")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        return adapter.create(RetrofitBackendDefinitionV1.class);
    }
}
