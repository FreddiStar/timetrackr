package de.almostintelligent.timetrackr.client.app;

import de.almostintelligent.timetrackr.client.controller.AppController;

public class TimeTrackrClientApplication {

    private static TimeTrackrClientApplication instance;

    private AppController appController;

    public static TimeTrackrClientApplication getInstance() {

        if (instance == null) {
            instance = new TimeTrackrClientApplication();
        }

        return instance;
    }

    private TimeTrackrClientApplication() {

    }

    public AppController getAppController() {
        return appController;
    }

    public void setAppController(AppController appController) {
        this.appController = appController;
    }

}
