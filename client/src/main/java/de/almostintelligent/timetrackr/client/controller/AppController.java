package de.almostintelligent.timetrackr.client.controller;

import de.almostintelligent.timetrackr.client.Main;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;

import java.io.IOException;

public class AppController {

    @FXML
    private StackPane appRoot;

    @FXML
    private Label messageFooter;

    public AppController() {
    }

    public void addLayer(Node node) {
        appRoot.getChildren().add(node);
        node.resize(appRoot.getWidth(), appRoot.getHeight());
    }

    public void setFooterMessage(String msg) {
        Platform.runLater(() -> {
            messageFooter.setText(msg);
        });

    }

    private void showLogin() throws IOException {
        addLayer(FXMLLoader.load(Main.class.getResource("login.fxml")));
    }

    @FXML
    private void initialize() throws IOException {
        addLayer(FXMLLoader.load(Main.class.getResource("main.fxml")));
    }

    public void removeLayer(Node node) {
        appRoot.getChildren().remove(node);
    }
}
