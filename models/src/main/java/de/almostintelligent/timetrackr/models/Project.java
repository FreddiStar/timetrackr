package de.almostintelligent.timetrackr.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "project")
public class Project {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    private Long createdAt;

    @JsonIgnore
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(targetEntity = TrackedTime.class, mappedBy = "project")
    private Set<TrackedTime> trackedTimes;

    @JsonIgnoreProperties({"project"})
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(targetEntity = Category.class, mappedBy = "project")
    private Set<Category> categories;

    public Project() {
        categories = new HashSet<Category>();
        trackedTimes = new HashSet<TrackedTime>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public void addCategory(Category cat) {
        categories.add(cat);
    }

    public Set<TrackedTime> getTrackedTimes() {
        return trackedTimes;
    }

    public void setTrackedTimes(Set<TrackedTime> trackedTimes) {
        this.trackedTimes = trackedTimes;
    }

    public void addTrackedTime(TrackedTime time) {
        trackedTimes.add(time);
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }
}
