package de.almostintelligent.timetrackr.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.Hibernate;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "category")
public class Category {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    private Long createdAt;

    @JsonIgnoreProperties({"categories"})
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToOne(targetEntity = Project.class)
    @JoinColumn(name = "project_id")
    private Project project;

    @JsonIgnore
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(targetEntity = TrackedTime.class)
    @JoinTable(name = "category_to_trackedtime",
            joinColumns = {@JoinColumn(name = "category_id")},
            inverseJoinColumns = {@JoinColumn(name = "trackedtime_id")})
    private Set<TrackedTime> trackedTimes;

    public Category() {
        Hibernate.initialize(trackedTimes);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Set<TrackedTime> getTrackedTimes() {
        return trackedTimes;
    }

    public void setTrackedTimes(Set<TrackedTime> trackedTimes) {
        this.trackedTimes = trackedTimes;
    }

    public void addTrackedTime(TrackedTime time) {
        trackedTimes.add(time);
    }
}
